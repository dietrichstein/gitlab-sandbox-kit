# Contributing

## Contributing to GitLab Sandbox Kit

The source of GitLab Sandbox Kit is [hosted on GitLab.com](https://gitlab.com/dietrichstein/gitlab-sandbox-kit/) and contributions are welcome!

## Contributor License Agreement

By submitting code as an individual you agree to the [Individual Contributor License Agreement](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/legal/individual_contributor_license_agreement.md).

By submitting code as an entity you agree to the [Corporate Contributor License Agreement](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/legal/corporate_contributor_license_agreement.md).
