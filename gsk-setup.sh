#!/bin/bash

# exit on first error (same as "set -e")
# set -o errexit

# exit | when | any | command | in | a | pipe | has | exitcode != 0
# set -o pipefail

# exit when an undefined variable is accessed (same as "set -u")
# set -o nounset

# print all lines for debugging (same as "set -x")
# set -o xtrace

# shopt -s nullglob

# set -m

# @TODO: Source a 'gsk-colors.sh' file
NORMAL="\\033[0;39m"
YELLOW="\\033[0;33m"

log() {
  echo -e "\n${YELLOW}$1 ${NORMAL}\n"
}

# This file should only exist when portions of this script have already run once
DOCKER_SETUP_FILE=".gsk-setup-once"

# This file needs to be copied locally from the DOCKER_HOST_PATH
DOCKER_SYSTEMD_FILE="systemctl3.py"

check_git_config() {
  log "Configuring git..."

  #printenv

  git_user="$(git config --global --get user.name)"
  if [[ $? != 0 ]] || [ "${git_user:-}" != $GIT_CONFIG_USER_NAME ]; then
    log "Updating git global value for user.name..."
    git config --global user.name $GIT_CONFIG_USER_NAME
  fi

  git_email="$(git config --global --get user.email)"
  if [[ $? != 0 ]] || [ "${git_email:-}" != $GIT_CONFIG_USER_EMAIL ]; then
    log "Updating git global value for user.email..."
    git config --global user.email $GIT_CONFIG_USER_EMAIL
  fi
}

check_ssh() {
  log "Checking for local SSH keys..."

  if [ ! -f /home/$USER/.ssh ]; then
    log "Installing ssh configuration..."
    mkdir -p /home/$USER/.ssh

    touch /home/$USER/.ssh/.config
    echo "# GitLab Work Account" >> /home/$USER/.ssh/.config
    echo "Host gitlab.com" >> /home/$USER/.ssh/.config
    echo "  HostName gitlab.com" >> /home/$USER/.ssh/.config
    echo "  User git" >> /home/$USER/.ssh/.config
    echo "  IdentityFile /home/$USER/.ssh/$DOCKER_SSH_PRIVATEKEY_FILE" >> /home/$USER/.ssh/.config
    sudo chmod 700 ~/.ssh

    touch /home/$USER/.ssh/known_hosts
    echo "|1|hunNLJ6DlUMdWgxWXp+U2fswNFk=|qfsAHw59R5tmYkGOH64vc5sOO0E= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" >> /home/$USER/.ssh/known_hosts
    echo "|1|shAnE927tcrmkm8rhI65RZ3W2sk=|Do+5/W0fXl3dKMe4TAkILVUZe6Y= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" >> /home/$USER/.ssh/known_hosts
    sudo chmod 644 /home/$USER/.ssh/known_hosts
  fi

  # Install private SSH key
  if [ ! -f /home/$USER/.ssh/$DOCKER_SSH_PRIVATEKEY_FILE ]; then
    log "Installing private key file for SSH..."
    cp $DOCKER_HOST_SSH_PRIVATEKEY_PATH /home/$USER/.ssh/$DOCKER_SSH_PRIVATEKEY_FILE
    sudo chmod 600 /home/$USER/.ssh/$DOCKER_SSH_PRIVATEKEY_FILE
  fi

  # Install public SSH key
  if [ ! -f /home/$USER/.ssh/$DOCKER_SSH_PUBLICKEY_FILE ]; then
    log "Installing public key file for SSH..."
    cp $DOCKER_HOST_SSH_PUBLICKEY_PATH /home/$USER/.ssh/$DOCKER_SSH_PUBLICKEY_FILE
    sudo chmod 644 /home/$USER/.ssh/$DOCKER_SSH_PUBLICKEY_FILE
  fi

  ssh -i /home/$USER/.ssh/$DOCKER_SSH_PRIVATEKEY_FILE -T git@gitlab.com
}

check_path() {
  # Something broke the "local/rvm" entries in PATH, was it node 12? For now we just fix it up.
  declare -a arr=("/usr/local/rvm/gems/ruby-2.6.3@global/bin"
                  "/usr/local/rvm/rubies/ruby-2.6.3/bin"
                  "/usr/local/rvm/bin"
                  "/usr/local/sbin"
                  "/usr/local/bin"
                  "/usr/sbin"
                  "/usr/bin"
                  "/sbin"
                  "/bin"
                  )

  for i in "${arr[@]}"
  do
    # or do whatever with individual element of the array
    if [[ ":$PATH:" != *":$i:"* ]]; then
      log "Adding PATH location: $i"
      PATH="${PATH:+${PATH}:}$i"
    fi
  done

  export PATH
}

# check_services() {
#   log "Installing required services..."
#   # Configure postgres and redis to be always running (does this persist? it doesn't start. consider supervisord)
#   sudo update-rc.d postgresql enable
#   sudo update-rc.d redis-server enable
#   sudo update-rc.d postfix enable
#   sudo service redis-server start
#   sudo service postgresql start
#   sudo service postfix start
#   # log "Run with: gitlab-runner --log-level debug run --config /etc/gitlab-runner/config.toml"
# }

# Perform tasks that should always run
run_always() {
  DEBIAN_FRONTEND=readline

  USER="$(whoami)"

  if [ "${USER}" != "$DOCKER_USER" ]; then
    log "Docker User: "$DOCKER_USER
    log "Current User: "$USER
    log "Changing to user: "$DOCKER_USER

    # su $DOCKER_USER -c "/etc/gsk/gsk-setup.sh run_always ; bash --login"
    su $DOCKER_USER -c "/etc/gsk/gsk-setup.sh run_always"

    # su $DOCKER_USER -c "bash --login -c '/etc/gsk/gsk-setup.sh run_always'"
  else
    source ~/.profile

    # Perhaps it would be better just to write this kind of thing somewhere to source it?
    check_path

    check_git_config

    # Needed under macOS due to Docker so that GDK does this:
    # ｢wds｣: Project is running at http://172.17.0.2:3809/
    #hostAddress=$(getent hosts $HOSTNAME | awk '{ print $1 }')
    #export DEV_SERVER_HOST=$hostAddress
    export DEV_SERVER_HOST=0.0.0.0
    #log "DEV_SERVER_HOST: "$DEV_SERVER_HOST
  fi

  bash --login
}

# Perform one-time setup tasks
run_once() {
  # Load in sysctl settings from /etc/sysctl.conf
  echo "fs.inotify.max_user_watches=524288" | tee -a /etc/sysctl.conf
  sysctl -p

  # Install systemctl stand-in (docker doesn't play nice with systemd)
  if [ ! -f $DOCKER_HOST_PATH/$DOCKER_SYSTEMD_FILE ]; then
    mv /usr/bin/systemctl /usr/bin/systemctl.bak
  fi
  cp $DOCKER_HOST_PATH/$DOCKER_SYSTEMD_FILE /usr/bin/systemctl
  test -L /bin/systemctl || ln -sf /usr/bin/systemctl /bin/systemctl

  touch $DOCKER_HOST_PATH/$DOCKER_SETUP_FILE

  log "Setup is now complete."
}

init() {
  log "Initalizing..."
  if [ ! -f $DOCKER_HOST_PATH/$DOCKER_SETUP_FILE ]; then
    log "Completing docker setup..."
    run_once
  fi
  run_always
}

"$@"
