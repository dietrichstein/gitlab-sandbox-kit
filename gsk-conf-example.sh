#!/bin/bash

# BUILD-TIME CONFIGURATION
# ------------------------------------------------------------------------------
# Go ahead and customize these ones right away.

# Network (macOS & WSL/Docker)
export USER_HOST_EE="ee.gitlab"
export USER_PORT_EE="3001"
export USER_HOST_CE="ce.gitlab"
export USER_PORT_CE="3000"

# Shell (macOS & WSL/Docker)
#export USER_PROFILE="${HOME}/.bash_profile"
#export USER_SHELL_COMMAND="bash"

export USER_PROFILE="${HOME}/.zshrc"
export USER_SHELL_COMMAND="zsh"

export USER_GSK_ALIAS_COMMAND="gsk"

# Installation (macOS & WSL/Docker)
export USER_PROJECTS_PATH="${HOME}"
export USER_GSK_PATH="${USER_PROJECTS_PATH}/gitlab-sandbox-kit"
export USER_EE_PATH="${USER_PROJECTS_PATH}/gdk-ee"
export USER_CE_PATH="${USER_PROJECTS_PATH}/gdk-ce"

# Git (macOS & WSL/Docker)
export USER_GITLAB_USER_NAME="YOUR_GITLAB_USER_NAME"
export USER_GITLAB_USER_EMAIL="YOUR GITLAB_EMAIL_ADDRESS"

# SSH (macOS & WSL/Docker)
# These are the paths to your private and public SSH keys.
# They can be host-mounted via WSL or local to your WSL distribution.
export USER_PRIVATEKEY_PATH="${HOME}/.ssh/YOUR_PRIVATE_SSH_KEY"
export USER_PUBLICKEY_PATH="${HOME}/.ssh/YOUR_PUBLIC_SSH_KEY.pub"

# SSH - Copied Filenames (WSL/Docker Only)
# These are the file names that will be used when copying your keys into the container.
export USER_PRIVATEKEY_DEST_FILENAME="id_ed25519"
export USER_PUBLICKEY_DEST_FILENAME="id_ed25519.pub"

# RUN-TIME CONFIGURATION
# ------------------------------------------------------------------------------
# Run "gsk run" at least once to get these values from your local instance.

# GitLab Runner (WSL/Docker Only)
export USER_EE_RUNNER_TOKEN="YOUR_GITLAB_EE_RUNNER_TOKEN"
export USER_CE_RUNNER_TOKEN="YOUR_GITLAB_CE_RUNNER_TOKEN"

# GitLab Runner (macOS Only)
export MACOS_EE_RUNNER_TOKEN="YOUR_GITLAB_EE_RUNNER_TOKEN"
export MACOS_EE_MUX="stmux-ee.zsh-example.pegjs"

# GitLab CE (macOS Only)
export MACOS_CE_RUNNER_TOKEN="YOUR_GITLAB_CE_RUNNER_TOKEN"
export MACOS_CE_MUX="stmux-ce.zsh-example.pegjs"
