### macOS HOSTS File Configuration

Use your CLI editor of choice to edit `/etc/hosts`. If you want something easy you could try installing micro:

```
cd /usr/local/bin
curl https://getmic.ro | bash
```

If you decide to try it, next run this:

```
sudo micro /etc/hosts
```

If not, use `nano` or another and run something like this:

```
sudo nano /etc/hosts
```

Add the following:

```
127.0.0.1 ce.gitlab
127.0.0.1 ee.gitlab
```

### Preparing Your Login Shell

#### Requirements
- Xcode
- Xcode CLI Tools
- An editor like nano, micro, vim, etc.
- A terminal such as Terminal, Term2, or Hyper

If you have already been using your system for development prior to installing the GSK, chances are good that you already have your shell set up just the way you like it. If this is the case you can disregard this section. However, if your on a freshly installed copy of macOS then it is highly probably that you don't have many of the standard shell files configured that make terminal-based development far more enjoyable. Before you continue, so check the hidden files inside your user folder and look for the following files:

- `.profile`
- `.bash_profile`
- `.bashrc`
- `.zshrc`

If you have some or all of these files already then you can at least alias commands already and shouldn't bother with the rest of this section.

As of macOS High Sierra, both Terminal and iTerm want to load ~/.profile first. Although, a fresh install of macOS doesn't come with ~/.profile either. So, the first thing to do on a fresh install is to create the files we need and set appropriate permissions:

```
touch ~/.profile
chmod 700 ~/.profile
```

```
touch ~/.bash_profile
chmod 700 ~/.bash_profile
```

Next, edit your `.profile` to source your `.bash_profile`:

```
nano ~/.profile
```

Add:

```
source ~/.bash_profile
```

### Preparing Your SSH Keys

### Configuring Your Environment Variables