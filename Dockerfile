FROM ubuntu:18.04 AS gsk-ubuntu
LABEL authors.maintainer "GSK contributors: https://gitlab.com/dietrichstein/gitlab-sandbox-kit/graphs/master"

FROM gsk-ubuntu AS gsk-base
COPY --from=gsk-ubuntu / /

# Environtment Variables
# Note: Override these extenerally at *build* time with "docker build --build-arg KEY=VALUE".
# Note: Override these extenerally at *run* time with "docker run -e KEY=VALUE".

ARG DEBIAN_FRONTEND=noninteractive
ENV DEBIAN_FRONTEND="${DEBIAN_FRONTEND}"

ARG APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=DontWarn
ENV APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE="${APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE}"

ARG ENABLE_LIVERELOAD=1
ENV ENABLE_LIVERELOAD="${ENABLE_LIVERELOAD}"

ARG CI_BUILD_REF_NAME=master
ENV CI_BUILD_REF_NAME="${CI_BUILD_REF_NAME}"

# Note: This avoids "ERROR: invoke-rc.d: could not determine current runlevel".
ARG RUNLEVEL=1
ENV RUNLEVEL="${RUNLEVEL}"

# Note: This avoids "ERROR: invoke-rc.d: could not determine current runlevel" for redis, et cetera.
RUN sed -i "s/^exit 101$/exit 0/" /usr/sbin/policy-rc.d

RUN apt-get update -qq

# Base Installation Requirements
COPY gsk-packages-0-base.txt /
RUN apt-get update && apt-get install -y --no-install-recommends $(sed -e 's/#.*//' /gsk-packages-0-base.txt)

# Repository Requirements (Base Dependent)
FROM gsk-base AS gsk-required
COPY --from=gsk-base / /

RUN add-apt-repository -y ppa:git-core/ppa
RUN add-apt-repository -y ppa:longsleep/golang-backports
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN curl -sS https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
RUN echo "deb https://deb.nodesource.com/node_12.x bionic main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt-cache madison nodejs
RUN apt-get update -qq
RUN apt-get upgrade -qq

# Timezone -- "Time is an illusion. Lunchtime, doubly so."
RUN echo "Etc/UTC" | tee /etc/timezone
RUN apt-get install -y --no-install-recommends tzdata

# Prepare For Postfix Install
SHELL ["/bin/bash", "-c", "-l"]
RUN debconf-set-selections <<< "postfix postfix/mailname string mailer.gitlab" > /dev/null
RUN debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'" > /dev/null

# Required Installation Packages
COPY gsk-packages-1-required.txt /
RUN apt-get update && apt-get install -y --no-install-recommends $(sed -e 's/#.*//' /gsk-packages-1-required.txt)

FROM gsk-required AS gsk-rvm-ruby
COPY --from=gsk-required / /

# Create "gdk" user and "gdk" group (gdk will create non-login "git" user for accessing gitlab-shell)
RUN useradd \
    --groups sudo \
    --uid 1000 \
    --shell /bin/bash \
    --create-home \
    --user-group gdk
RUN echo "gdk ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/gdk

# RVM Installation
# RUN gpg2 \
#   --keyserver hkp://pool.sks-keyservers.net \
#   --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
RUN curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -
RUN curl -sSL https://rvm.io/pkuczynski.asc | gpg2 --import -
RUN sudo curl -sSL https://raw.githubusercontent.com/rvm/rvm/stable/binscripts/rvm-installer -o rvm-installer
# RUN sudo curl -sSL https://raw.githubusercontent.com/rvm/rvm/stable/binscripts/rvm-installer.asc -o rvm-installer.asc
# RUN gpg2 --verify rvm-installer.asc rvm-installer
RUN chmod a+x rvm-installer
RUN ./rvm-installer stable
RUN usermod -a -G rvm gdk

USER gdk

# RVM & Ruby Configuration
RUN rvm requirements
RUN rvm install 2.6.3
RUN rvm use 2.6.3@global --default
RUN sudo chown -R gdk:gdk /usr/local/rvm/rubies/ruby-2.6.3/lib/ruby/gems/2.6.0
RUN sudo chown -R gdk:gdk /usr/local/rvm/rubies/ruby-2.6.3/bin

FROM gsk-rvm-ruby AS gsk-gems
COPY --from=gsk-rvm-ruby / /

# Middleman Gem
RUN gem install middleman --no-document

# Bundler Gem -- Workaround with "--force" bypasses this error:
RUN gem install bundler -v 1.17.3 --force --no-document

# GitLab Shell Dependency Gems
RUN mkdir -p /home/gdk/gitlab-development-kit/gitlab-shell
WORKDIR /home/gdk/gitlab-development-kit/gitlab-shell
RUN sudo curl -OO https://gitlab.com/gitlab-org/gitlab-shell/raw/master/{Gemfile,Gemfile.lock} && \
    bundle _1.17.3_ install --without production --jobs 4

# Gitaly Dependency Gems
RUN mkdir -p /home/gdk/gitlab-development-kit/gitaly/ruby
WORKDIR /home/gdk/gitlab-development-kit/gitaly/ruby
RUN sudo curl -OO https://gitlab.com/gitlab-org/gitaly/raw/master/ruby/{Gemfile,Gemfile.lock} && \
    bundle _1.17.3_ install --jobs 4 && rm Gemfile Gemfile.lock

# GitLab CE Dependency Gems
RUN mkdir -p /home/gdk/gitlab-development-kit/gitlab
WORKDIR /home/gdk/gitlab-development-kit/gitlab
RUN sudo curl -OO https://gitlab.com/gitlab-org/gitlab-ce/raw/master/{Gemfile,Gemfile.lock}
RUN bundle _1.17.3_ install --without mysql production --jobs 4

# GitLab EE Dependency Gems
RUN mkdir -p /home/gdk/gitlab-development-kit/gitlab-ee
WORKDIR /home/gdk/gitlab-development-kit/gitlab-ee
RUN sudo curl -OO https://gitlab.com/gitlab-org/gitlab-ee/raw/master/{Gemfile,Gemfile.lock}
RUN bundle _1.17.3_ install --without mysql production --jobs 4

# Dependencies are now installed, so we can delete our temporary folder
WORKDIR /home/gdk
RUN sudo rm -rf gitlab-development-kit

# GDK Gem
RUN gem install gitlab-development-kit

# GitLab EE Runner
FROM gsk-gems AS gsk-runner
COPY --from=gsk-gems / /
RUN sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
RUN sudo chmod +x /usr/local/bin/gitlab-runner
RUN sudo useradd \
    --groups sudo \
    --uid 1001 \
    --shell /bin/bash \
    --create-home \
    --user-group gitlab-runner
RUN sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

# GitLab EE
FROM gsk-runner AS gsk-ee
COPY --from=gsk-runner / /
RUN mkdir -p /home/gdk/gitlab-ee
WORKDIR /home/gdk/gitlab-ee
RUN gdk init .
RUN echo ee.gitlab > hostname && \
    echo 0.0.0.0 > host && \
    echo 3001 > port && \
    echo 3809 > webpack_port && \
    echo 5432 > postgresql_port && \
    echo 5432 > postgresql_geo_port && \
    echo 5000 > registry_port && \
    echo 3011 > gitlab_pages_port
RUN yarn cache clean
RUN sudo rm -rf /home/gdk/.cache/
RUN gdk install gitlab_repo=https://gitlab.com/gitlab-org/gitlab-ee.git

# GitLab CE
# FROM gsk-ee AS gsk-ce
# COPY --from=gsk-ee / /
# RUN mkdir -p /home/gdk/gitlab-ce
# WORKDIR /home/gdk/gitlab-ce
# RUN gdk init .
# RUN echo ce.gitlab > hostname && \
#     echo 0.0.0.0 > host && \
#     echo 3000 > port && \
#     echo 3808 > webpack_port && \
#     echo 5432 > postgresql_port && \
#     echo 5432 > postgresql_geo_port && \
#     echo 5000 > registry_port && \
#     echo 3010 > gitlab_pages_port
# RUN yarn cache clean
# RUN sudo rm -rf /home/gdk/.cache/
# RUN gdk install gitlab_repo=https://gitlab.com/gitlab-org/gitlab-ce.git

# FROM gsk-ce AS gsk-convenience
# COPY --from=gsk-ce / /

FROM gsk-ee AS gsk-convenience
COPY --from=gsk-ee / /

# Switch to the "root" so that initial script-based setup can run and then change to the "gdk" user
USER root

# Convenience Utilities
RUN cd /usr/bin; curl https://getmic.ro | bash
COPY gsk-packages-2-convenience.txt /
RUN apt-get update && apt-get install -y --no-install-recommends $(sed -e 's/#.*//' /gsk-packages-2-convenience.txt)

WORKDIR /home/gdk

ARG EDITOR=micro
ENV EDITOR="${EDITOR}"

ARG EE_RUNNER_TOKEN=""
ENV EE_RUNNER_TOKEN="${EE_RUNNER_TOKEN}"

ARG EE_RUNNER_URL=""
ENV EE_RUNNER_URL="${EE_RUNNER_URL}"

ARG CE_RUNNER_TOKEN=""
ENV CE_RUNNER_TOKEN="${CE_RUNNER_TOKEN}"

ARG CE_RUNNER_URL=""
ENV CE_RUNNER_URL="${CE_RUNNER_URL}"

FROM gsk-convenience AS gsk-final
COPY --from=gsk-convenience / /
