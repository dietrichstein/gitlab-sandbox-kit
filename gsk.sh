#!/bin/bash

# exit on first error (same as "set -e")
# set -o errexit

# exit | when | any | command | in | a | pipe | has | exitcode != 0
# set -o pipefail

# exit when an undefined variable is accessed (same as "set -u")
# set -o nounset

# print all lines for debugging (same as "set -x")
# set -o xtrace

# shopt -s nullglob

# set -m

# Colors
# @TODO: Source a 'gsk-colors.sh' file
NORMAL="\\033[0;39m"
RED="\\033[1;31m"
YELLOW="\\033[1;33m"
GREEN="\\033[1;32m"
CYAN="\\033[1;36m"
BLUE="\\033[1;34m"
PURPLE="\\033[1;35m"

_color_test() {
  _log "\x00\xe2\x96\x88" $RED "\n" "" "-n"
  _log "\x00\xe2\x96\x88" $YELLOW "" "" "-n"
  _log "\x00\xe2\x96\x88" $GREEN "" "" "-n"
  _log "\x00\xe2\x96\x88" $CYAN "" "" "-n"
  _log "\x00\xe2\x96\x88" $BLUE "" "" "-n"
  _log "\x00\xe2\x96\x88" $PURPLE "" "" "-n"
}

_log() {
  local COLOR="${2:-$NORMAL}"
  local PREFIX="${3:+\n}"
  local SUFFIX="${4:+\n}"
  local ARGS=" ${5:-}"
  local TEST="${6:-0}"
  if [ "$TEST" -eq 1 ]; then
    _color_test
  fi
  echo -e $ARGS "${COLOR}${PREFIX}$1${SUFFIX}${NORMAL}"
}

_error() {
  _log "ERROR: $1" $RED "\n" "\n"
}

_hr() {
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
}

# Avoid dependence on dirname for terminal emulators
script_path=${0%/*}

# Source a small MIT-licensed library for keyboard-based option selection
source "$script_path/lib/sind/sind-modified.sh"

# Source the user configured variables
script_config="$script_path/gsk-conf.sh"
if [[ ! -f "$script_config" ]]; then
  _error "Please copy the 'gsk-conf-example.sh' file to 'gsk-conf.sh' and configure the variables within."
  exit 0
fi
. "$script_path/gsk-conf.sh"

# Runner URLs -- Modification is not recommended.
export USER_EE_RUNNER_URL="http://${USER_HOST_EE}:${USER_PORT_EE}/"
export USER_CE_RUNNER_URL="http://${USER_HOST_EE}:${USER_PORT_CE}/"

# Docker Variables -- Modification is not recommended.
export DOCKER_USER="gdk"
export DOCKER_FILE="Dockerfile"
export DOCKER_META="gsk"
export DOCKER_SETUP_PATH="/etc/${DOCKER_META}"
export DOCKER_IMAGE_NAME="${DOCKER_META}-final"
export DOCKER_CONTAINER_NAME="${DOCKER_META}-container"
export DOCKER_HOMEDIR="/home/${DOCKER_USER}"

help() {
  echo "╔══════════════════════════════════════════════════════════════════════════════╗"
  echo "║  GSK - GitLab Sandbox Kit - Help                                             ║"
  echo "╠══════════════════════════════════════════════════════════════════════════════╣"
  echo "║                                                                              ║"
  echo "║  Usage:    ./gsk.sh <command>                                                ║"
  echo "║            gsk <command>                                                     ║"
  echo "║                                                                              ║"
  echo "║  Note: To enable the 'gsk <command>' syntax you must first make this script  ║"
  echo "║  executable. To get started quickly, first make a copy of the file named     ║"
  echo "║  'gsk-conf-example.sh' with the name 'gsk-conf.sh'. Then, configure the      ║"
  echo "║  USER_PROFILE variable within the copy you created. Next. run the commands   ║"
  echo "║  shown below and open a new terminal or execute 'source ~/.bash_profile' or  ║"
  echo "║  the equivalent matching your configured USER_PROFILE value. If you aren't   ║"
  echo "║  using bash as your shell you may have to add the alias manually.            ║"
  echo "║                                                                              ║"
  echo "║            chmod a+x ./gsk.sh                                                ║"
  echo "║            ./gsk.sh add_alias                                                ║"
  echo "║                                                                              ║"
  echo "║  General Commands:                                                           ║"
  echo "║                                                                              ║"
  echo "║  help      Shows this help information                                       ║"
  echo "║  next      Shows some helpful tips on what to next after your install        ║"
  echo "║  add_alias Creates an alias to the gsk.sh script in your user profile        ║"
  echo "║                                                                              ║"
  echo "║  WSL/Docker Commands:                                                        ║"
  echo "║                                                                              ║"
  echo "║  build     Builds the image configured by DOCKER_IMAGE_NAME                  ║"
  echo "║  run       Runs the container configured by DOCKER_CONTAINER_NAME            ║"
  echo "║  shell     Opens a shell into a running DOCKER_CONTAINER_NAME container      ║"
  echo "║  stop      Stops the container configured by DOCKER_CONTAINER_NAME           ║"
  echo "║  remove    Removes the container configured by DOCKER_CONTAINER_NAME         ║"
  echo "║  clean     Attempts to reclaim space without removing important images       ║"
  echo "║  nuke      Permanantly removes all of your containers and images             ║"
  echo "║  nav       Changes your working directory to your USER_GSK_PATH              ║"
  echo "║                                                                              ║"
  echo "║  macOS Commands:                                                             ║"
  echo "║                                                                              ║"
  echo "║  macos              Begins a guided install of EE and/or CE for macOS user   ║"
  echo "║                                                                              ║"
  echo "║  macos ee-mux       Opens a multiplexed terminal layout using MACOS_EE_MUX   ║"
  echo "║  macos ee-gdk       Executes 'gdk run' for the EE installation               ║"
  echo "║  macos ee-runner    Executes the EE runner and registers it if needed        ║"
  echo "║                                                                              ║"
  echo "║  macos ce-mux       Opens a multiplexed terminal layout using MACOS_CE_MUX   ║"
  echo "║  macos ce-gdk       Executes 'gdk run' for the CE installation               ║"
  echo "║  macos ce-runner    Executes the CE runner and registers it if needed        ║"
  echo "║                                                                              ║"
  echo "╚══════════════════════════════════════════════════════════════════════════════╝"
}

next() {
  echo "╔══════════════════════════════════════════════════════════════════════════════╗"
  echo "║  GSK - GitLab Sandbox Kit - Next Steps                                       ║"
  echo "╠══════════════════════════════════════════════════════════════════════════════╣"
  echo "║                                                                              ║"
  echo "║  1) Enter your installation directory and execute 'gdk run'.                 ║"
  echo "║                                                                              ║"
  echo "║  2) Open your browser to 'http://<USER_HOST>:<USER_PORT>' and sign in with:  ║"
  echo "║                                                                              ║"
  echo "║            User = root                                                       ║"
  echo "║            Login = 5iveL!fe                                                  ║"
  echo "║                                                                              ║"
  echo "║  3) Click the 'Admin Area' wrench icon at the top and then 'Runners' in the  ║"
  echo "║     sidebar to get your runner token for this installation. Paste it into    ║"
  echo "║     your 'gsk-conf.sh' for safe-keeping.                                   ║"
  echo "║                                                                              ║"
  echo "╚══════════════════════════════════════════════════════════════════════════════╝"
}

build() {
  _log "BUILD -- Building images..."

  # We must build from the script location so that docker commands like COPY receive the correct context
  cd $( dirname "${BASH_SOURCE[0]}")

  SECONDS=0

  SECONDS_UBUNTU=$SECONDS
  currentImage="${DOCKER_META}-ubuntu"
  DOCKER_BUILDKIT=1 docker build \
    --force-rm \
    --target $currentImage \
    --tag="$currentImage:latest" \
    --label="${DOCKER_META}=1" \
    --progress=plain \
    --file "${script_path}/${DOCKER_FILE}" \
    .
  SECONDS_UBUNTU=$(($SECONDS - $SECONDS_UBUNTU))
  _log "Built '$currentImage' image in $(($SECONDS_UBUNTU / 3600)) hours, $((($SECONDS_UBUNTU / 60) % 60)) minutes, and $(($SECONDS_UBUNTU % 60)) seconds."

  SECONDS_BASE=$SECONDS
  currentImage="${DOCKER_META}-base"
  DOCKER_BUILDKIT=1 docker build \
    --force-rm \
    --target $currentImage \
    --tag="$currentImage:latest" \
    --label="${DOCKER_META}=1" \
    --progress=plain \
    --file "${script_path}/${DOCKER_FILE}" \
    .
  SECONDS_BASE=$(($SECONDS - $SECONDS_BASE))
  _log "Built '$currentImage' image in $(($SECONDS_BASE / 3600)) hours, $((($SECONDS_BASE / 60) % 60)) minutes, and $(($SECONDS_BASE % 60)) seconds."

  SECONDS_REQUIRED=$SECONDS
  currentImage="${DOCKER_META}-required"
  DOCKER_BUILDKIT=1 docker build \
    --force-rm \
    --target $currentImage \
    --tag="$currentImage:latest" \
    --label="${DOCKER_META}=1" \
    --progress=plain \
    --file "${script_path}/${DOCKER_FILE}" \
    .
  SECONDS_REQUIRED=$(($SECONDS - $SECONDS_REQUIRED))
  _log "Built '$currentImage' image in $(($SECONDS_REQUIRED / 3600)) hours, $((($SECONDS_REQUIRED / 60) % 60)) minutes, and $(($SECONDS_REQUIRED % 60)) seconds."

  SECONDS_RVM_RUBY=$SECONDS
  currentImage="${DOCKER_META}-rvm-ruby"
  DOCKER_BUILDKIT=1 docker build \
    --force-rm \
    --target $currentImage \
    --tag="$currentImage:latest" \
    --label="${DOCKER_META}=1" \
    --progress=plain \
    --file "${script_path}/${DOCKER_FILE}" \
    .
  SECONDS_RVM_RUBY=$(($SECONDS - $SECONDS_RVM_RUBY))
  _log "Built '$currentImage' image in $(($SECONDS_RVM_RUBY / 3600)) hours, $((($SECONDS_RVM_RUBY / 60) % 60)) minutes, and $(($SECONDS_RVM_RUBY % 60)) seconds."

  SECONDS_GEMS=$SECONDS
  currentImage="${DOCKER_META}-gems"
  DOCKER_BUILDKIT=1 docker build \
    --force-rm \
    --target $currentImage \
    --tag="$currentImage:latest" \
    --label="${DOCKER_META}=1" \
    --progress=plain \
    --file "${script_path}/${DOCKER_FILE}" \
    .
  SECONDS_GEMS=$(($SECONDS - $SECONDS_GEMS))
  _log "Built '$currentImage' image in $(($SECONDS_GEMS / 3600)) hours, $((($SECONDS_GEMS / 60) % 60)) minutes, and $(($SECONDS_GEMS % 60)) seconds."

  SECONDS_RUNNER=$SECONDS
  currentImage="${DOCKER_META}-runner"
  DOCKER_BUILDKIT=1 docker build \
    --force-rm \
    --target $currentImage \
    --tag="$currentImage:latest" \
    --label="${DOCKER_META}=1" \
    --progress=plain \
    --file "${script_path}/${DOCKER_FILE}" \
    .
  SECONDS_RUNNER=$(($SECONDS - $SECONDS_RUNNER))
  _log "Built '$currentImage' image in $(($SECONDS_RUNNER / 3600)) hours, $((($SECONDS_RUNNER / 60) % 60)) minutes, and $(($SECONDS_RUNNER % 60)) seconds."

  SECONDS_EE=$SECONDS
  currentImage="${DOCKER_META}-ee"
  DOCKER_BUILDKIT=1 docker build \
    --force-rm \
    --target $currentImage \
    --tag="$currentImage:latest" \
    --label="${DOCKER_META}=1" \
    --progress=plain \
    --file "${script_path}/${DOCKER_FILE}" \
    .
  SECONDS_EE=$(($SECONDS - $SECONDS_EE))
  _log "Built '$currentImage' image in $(($SECONDS_EE / 3600)) hours, $((($SECONDS_EE / 60) % 60)) minutes, and $(($SECONDS_EE % 60)) seconds."

  SECONDS_CE=$SECONDS
  currentImage="${DOCKER_META}-ce"
  DOCKER_BUILDKIT=1 docker build \
    --force-rm \
    --target $currentImage \
    --tag="$currentImage:latest" \
    --label="${DOCKER_META}=1" \
    --progress=plain \
    --file "${script_path}/${DOCKER_FILE}" \
    .
  SECONDS_CE=$(($SECONDS - $SECONDS_CE))
  _log "Built '$currentImage' image in $(($SECONDS_CE / 3600)) hours, $((($SECONDS_CE / 60) % 60)) minutes, and $(($SECONDS_CE % 60)) seconds."

  SECONDS_CONVENIENCE=$SECONDS
  currentImage="${DOCKER_META}-convenience"
  DOCKER_BUILDKIT=1 docker build \
    --force-rm \
    --target $currentImage \
    --tag="$currentImage:latest" \
    --label="${DOCKER_META}=1" \
    --progress=plain \
    --file "${script_path}/${DOCKER_FILE}" \
    .
  SECONDS_CONVENIENCE=$(($SECONDS - $SECONDS_CONVENIENCE))
  _log "Built '$currentImage' image in $(($SECONDS_CONVENIENCE / 3600)) hours, $((($SECONDS_CONVENIENCE / 60) % 60)) minutes, and $(($SECONDS_CONVENIENCE % 60)) seconds."

  SECONDS_FINAL=$SECONDS
  currentImage="${DOCKER_META}-final"
  DOCKER_BUILDKIT=1 docker build \
    --force-rm \
    --target $currentImage \
    --tag="$currentImage:latest" \
    --label="${DOCKER_META}=1" \
    --progress=plain \
    --file "${script_path}/${DOCKER_FILE}" \
    --add-host=${USER_HOST_EE}:127.0.0.1 \
    --add-host=${USER_HOST_CE}:127.0.0.1 \
    --add-host=postfix.gitlab.dev:127.0.0.1 \
    .
  SECONDS_FINAL=$(($SECONDS - $SECONDS_FINAL))
  _log "Built '$currentImage' image in $(($SECONDS_FINAL / 3600)) hours, $((($SECONDS_FINAL / 60) % 60)) minutes, and $(($SECONDS_FINAL % 60)) seconds."

  if [ $? != 0 ]; then
    _error "The docker images failed to build."
    exit 0
  fi

  _log "Built all images in $(($SECONDS / 3600)) hours, $((($SECONDS / 60) % 60)) minutes, and $(($SECONDS % 60)) seconds."
}

run() {
  _log "RUN -- Running container: ${DOCKER_CONTAINER_NAME}"

  chmod +x ./gsk-setup.sh
  chmod +x ./systemctl3.py

  # Mounting local drivers can can be useful for WSL
  #    -v /c:/c \

  # Untested for WSL, but necessart for webpack-dev-server under macOS
  #  --cap-add=NET_BIND_SERVICE \
  #  -v /var/run/docker.sock:/var/run/docker.sock \

  # Required for WSL
  #  -p 3809:3809 \
  #  -p 3808:3808 \
  #  -p 3011:3011 \
  #  -p 3010:3010 \
  #  -p 465:465 \
  #  -p 443:443 \
  #  -p 80:80 \
  #  -p 22:22 \

  # Note: Outside of WSL it would probably be preferrable to use "--network host" over port mapping
  #  --network="host" \

  #  -a stdin \
  #  -a stdout \
  #  -a stderr \

  docker run \
    --label ${DOCKER_META}=1 \
    --privileged \
    --cap-add=SYS_ADMIN \
    --rm \
    -i \
    -t \
    --cap-add=SYS_ADMIN \
    --cap-add=NET_BIND_SERVICE \
    --name ${DOCKER_CONTAINER_NAME} \
    --env GIT_CONFIG_USER_NAME=${USER_GITLAB_USER_NAME} \
    --env GIT_CONFIG_USER_EMAIL=${USER_GITLAB_USER_EMAIL} \
    --env DOCKER_HOST_SSH_PRIVATEKEY_PATH=${USER_PRIVATEKEY_PATH} \
    --env DOCKER_HOST_SSH_PUBLICKEY_PATH=${USER_PUBLICKEY_PATH} \
    --env DOCKER_SSH_PRIVATEKEY_FILE=${USER_PRIVATEKEY_DEST_FILENAME} \
    --env DOCKER_SSH_PUBLICKEY_FILE=${USER_PUBLICKEY_DEST_FILENAME} \
    --env DOCKER_USER=${DOCKER_USER} \
    --env DOCKER_HOST_PATH=${DOCKER_SETUP_PATH} \
    --env EE_RUNNER_TOKEN=${USER_EE_RUNNER_TOKEN} \
    --env EE_RUNNER_URL=${USER_EE_RUNNER_URL} \
    --env CE_RUNNER_TOKEN=${USER_CE_RUNNER_TOKEN} \
    --env CE_RUNNER_URL=${USER_CE_RUNNER_URL} \
    -p ${USER_PORT_EE}:${USER_PORT_EE} \
    -p ${USER_PORT_CE}:${USER_PORT_CE} \
    -p 3809:3809 \
    -p 3808:3808 \
    -p 3011:3011 \
    -p 3010:3010 \
    -p 465:465 \
    -p 443:443 \
    -p 80:80 \
    -p 22:22 \
    -v ${USER_GSK_PATH}:${DOCKER_SETUP_PATH} \
    -w=/${DOCKER_HOMEDIR} \
    ${DOCKER_IMAGE_NAME}:latest \
    ${DOCKER_SETUP_PATH}/gsk-setup.sh init
}

shell() {
  _log "SHELL -- Executing shell into container: ${DOCKER_CONTAINER_NAME}"
  docker exec -it ${DOCKER_CONTAINER_NAME} /bin/bash
}

stop() {
  _log "STOP -- Stopping container: ${DOCKER_CONTAINER_NAME}"
  docker stop ${DOCKER_CONTAINER_NAME}
}

remove() {
  _log "REMOVE -- Removing container: ${DOCKER_CONTAINER_NAME}"
  docker rm -f ${DOCKER_CONTAINER_NAME} &> /dev/null || true
}

clean() {
  _log "CLEAN"
  docker rmi "$(docker images --filter=dangling=true -q --no-trunc)"
}

nuke() {
  _log "NUKE"
  if [[ $(docker ps -a -q) != "" ]]; then
    docker rm $(docker ps -a -q)
  fi
  if [[ $(docker images -q) != "" ]]; then
    docker rmi $(docker images -q) --force
  fi
}

add_alias() {
  _log "ALIAS"
  echo alias ${USER_GSK_ALIAS_COMMAND}="${script_path}/gsk.sh" >> ${USER_PROFILE}
}

nav() {
  if ! [ -z $1 ] && [ $1 == "ee" ]; then
    _log "GITLAB-EE" $YELLOW " " " "
    cd ${USER_PROJECTS_PATH}/gdk-ee

  elif ! [ -z $1 ] && [ $1 == "ce" ]; then
    _log "GITLAB-CE" $YELLOW " " " "
    cd ${USER_PROJECTS_PATH}/gdk-ce

  else
    _log "GITLAB-GSK" $YELLOW " " " "
    cd $USER_GSK_PATH
  fi

  $USER_SHELL_COMMAND --login

  exit $?
}

macos_killall() {
  _log "Killing all processes..." $PURPLE "" ""
  # stmux doesn't seem to have named sessions so kill all the things

  killall \
    redis-server \
    postgres \
    ruby \
    node \
    gitaly \
    chromedriver \
    gitlab-workhorse \
    gitlab-runner \
    gitlab-runner-bleeding \
    unicorn_rails \
    jaeger-all-in-one

  pidfile_mailroom="${USER_PROJECTS_PATH}/gdk-$1/gitlab/tmp/pids/gitlab-mailroom.pid"
  if [[ -a $pidfile_mailroom ]]; then
    pid_mailroom=$(cat $pidfile_mailroom)
    if [[ $(ps aux | pgrep $pid_mailroom) ]]; then
      _log "Killing mailroom process: $pid_mailroom" $PURPLE "" ""
      kill -9 $pid_mailroom
    fi
  fi

  pidfile_unicorn="${USER_PROJECTS_PATH}/gdk-$1/gitlab/tmp/pids/unicorn.pid"
  if [[ -a $pidfile_unicorn ]]; then
    pid_unicorn=$(cat $pidfile_unicorn)
    if [[ $(ps aux | pgrep $pid_unicorn) ]]; then
      _log "Killing unicorn process: $pid_unicorn" $PURPLE "" ""
      kill -9 $pid_unicorn
    fi
  fi

  pidfile_sidekiq="${USER_PROJECTS_PATH}/gdk-$1/gitlab/tmp/pids/sidekiq.pid"
  if [[ -a $pidfile_sidekiq ]]; then
    pid_sidekiq=$(cat $pidfile_sidekiq)
    if [[ $(ps aux | pgrep $pid_sidekiq) ]]; then
      _log "Killing sidekiq process: $pid_sidekiq" $PURPLE "" ""
      kill -9 $pid_sidekiq
    fi
  fi

  pids=(`ps -aef | grep -E "jaeger" | grep -v grep | awk '{print $2}'`)
  for pid in $pids; do
    _log "Killing jaegar process: $pid" $PURPLE "" ""
    kill -9 $pid
  done

  pids=(`ps -aef | grep -E "webpack" | grep -v grep | awk '{print $2}'`)
  for pid in $pids; do
    _log "Killing webpack process: $pid" $PURPLE "" ""
    kill -9 $pid
  done

  pids=(`ps -aef | grep -E "gitlab-pages" | grep -v grep | awk '{print $2}'`)
  for pid in $pids; do
    _log "Killing gitlab-pages process: $pid" $PURPLE "" ""
    kill -9 $pid
  done

  pids=(`ps -aef | grep -E "${USER_PROJECTS_PATH}/gdk-$1" | grep -v grep | awk '{print $2}'`)
  for pid in $pids; do
    _log "Killing '$1' process: $pid" $PURPLE "" ""
    kill -9 $pid
  done
}

# Assisted GDK installation directly upon a macOS host
macos() {
  #set -o xtrace
  #set -o errexit
  #trap '_error "Installation has failed. Please review the output above for more information."' ERR

  if ! [ -z $1 ] && [ $1 == "ee-runner" ]; then
    _log "GITLAB-EE RUNNER..." $YELLOW " " " "
    cd ${USER_PROJECTS_PATH}/gdk-ee

    if ! [ -z $MACOS_EE_RUNNER_TOKEN ] && [ $MACOS_EE_RUNNER_TOKEN != "YOUR_GITLAB_EE_RUNNER_TOKEN" ]; then
      # If no config file exists allow time for gdk to have workers ready for registration
      if ! [ -f "${USER_PROJECTS_PATH}/gdk-ee/gitlab-runner-config.toml" ]; then
        _error "Did not find runner configuration at: ${USER_PROJECTS_PATH}/gdk-ee/gitlab-runner-config.toml"
        wait_secs=45
        while [ "$wait_secs" -gt 0 ]; do
          ((wait_secs--))
          echo -ne "Registering EE runner in $wait_secs seconds..."\\r
          sleep 1
        done

        gitlab-runner register \
          --config ${USER_PROJECTS_PATH}/gdk-ce/gitlab-runner-config.toml \
          --non-interactive \
          --url "http://ee.gitlab:3001/" \
          --registration-token "${MACOS_EE_RUNNER_TOKEN}" \
          --executor "shell" \
          --docker-image alpine:latest \
          --description "gdk-ee-runner" \
          --tag-list "gdk,dev,shell" \
          --run-untagged="true" \
          --locked="false"
      fi

      gitlab-runner --log-level debug run --config ${USER_PROJECTS_PATH}/gdk-ee/gitlab-runner-config.toml
    fi
    exit $?
  fi

  if ! [ -z $1 ] && [ $1 == "ce-runner" ]; then
    _log "GITLAB-CE RUNNER..." $YELLOW " " " "
    cd ${USER_PROJECTS_PATH}/gdk-ce

    if ! [ -z $MACOS_CE_RUNNER_TOKEN ] && [ $MACOS_CE_RUNNER_TOKEN != "YOUR_GITLAB_CE_RUNNER_TOKEN" ]; then
      # If no config file exists allow time for gdk to have workers ready for registration
      if ! [ -f "${USER_PROJECTS_PATH}/gdk-ce/gitlab-runner-config.toml" ]; then
        _error "Did not find runner configuration at: ${USER_PROJECTS_PATH}/gdk-ce/gitlab-runner-config.toml"
        wait_secs=45
        while [ "$wait_secs" -gt 0 ]; do
          ((wait_secs--))
          echo -ne "Registering CE runner in $wait_secs seconds..."\\r
          sleep 1
        done

        gitlab-runner register \
          --config ${USER_PROJECTS_PATH}/gdk-ce/gitlab-runner-config.toml \
          --non-interactive \
          --url "http://ce.gitlab:3000/" \
          --registration-token "${MACOS_CE_RUNNER_TOKEN}" \
          --executor "shell" \
          --docker-image alpine:latest \
          --description "gdk-ce-runner" \
          --tag-list "gdk,dev,shell" \
          --run-untagged="true" \
          --locked="false"
      fi

      gitlab-runner --log-level debug run --config ${USER_PROJECTS_PATH}/gdk-ce/gitlab-runner-config.toml
    fi
    exit $?
  fi

  if ! [ -z $1 ] && [ $1 == "ee-gdk" ]; then
    _log "GITLAB-EE GDK..." $YELLOW " " " "
    cd ${USER_PROJECTS_PATH}/gdk-ee
    macos_killall "ee"
    gdk run
    exit $?
  fi

  if ! [ -z $1 ] && [ $1 == "ce-gdk" ]; then
    _log "GITLAB-CE GDK..." $YELLOW " " " "
    cd ${USER_PROJECTS_PATH}/gdk-ce
    macos_killall "ce"
    gdk run
    exit $?
  fi

  #TODO: Check if stmux is installed and the config file exists
  # https://github.com/rse/stmux
  if ! [ -z $1 ] && [ $1 == "ee-mux" ]; then
    _log "GITLAB-EE MUX..." $YELLOW " " ""
    macos_killall "ee"
    stmux -M -w always -f $script_path/$MACOS_EE_MUX
    exit $?
  fi

  #TODO: Check if stmux is installed and the config file exists
  # https://github.com/rse/stmux
  if ! [ -z $1 ] && [ $1 == "ce-mux" ]; then
    _log "GITLAB-CE MUX..." $YELLOW " " ""
    macos_killall "ce"
    stmux -w always -f $script_path/$MACOS_CE_MUX
    exit $?
  fi

  init_select() {
    select_option "$@" 1>&2
    local result=$?
    echo $result
    return $result
  }

  setval() {
    printf -v "$1" "%s" "$(cat)"
    declare -p "$1"
  }

  log_versions() {
    echo -n "Brew:         " && brew --version
    echo -n "Git:          " && git --version
    echo -n "Node:         " && echo -n $(node -v) && echo " -- $(which node)"
    echo -n "Ruby:         " && ruby -v
    echo -n "Go:           " && go version
    echo -n "Redis:        " && redis-server --version
    echo -n "PostgreSQL:   " && pg_config --version
    echo -n "Chrome:       " && /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --version
    echo -n "ChromeDriver: " && chromedriver --version
    echo -n "Runner:       " && gitlab-runner --version
  }

  install_choice() {
    install_label=$(echo $1 | tr a-z A-Z)
    _log "GDK - GitLab $install_label - Installation" $CYAN "\n" ""
    _hr
    _log "Would you like to install GitLab $install_label now?" "" "" "\n\n"
    if [ $1 = "ee" ]; then
      case `init_select "Yes" "No"` in
        0) install_gdk "ee";;
        1) install_choice "ce";;
      esac
    elif [ $1 = "ce" ]; then
      case `init_select "Yes" "No"` in
        0) install_gdk "ce";;
        1) exit_final_handler;;
      esac
    fi
  }

  install_gdk() {
    default_folder="gdk-$1"

    #TODO: Trim any trailing slash from $default_dest

    if [ $1 = "ee" ] && [ -d $USER_EE_PATH ]; then
      default_install_path="$USER_EE_PATH"
    elif [ $1 = "ce" ] && [ -d $USER_CE_PATH ]; then
      default_install_path="$USER_CE_PATH"
    else
      if [ $1 = "ee" ] && [ -d $USER_PROJECTS_PATH ]; then
        default_install_path="$USER_PROJECTS_PATH/$default_folder"
      elif [ $1 = "ce" ] && [ -d $USER_PROJECTS_PATH ]; then
        default_install_path="$USER_PROJECTS_PATH/$default_folder"
      else
        default_install_path="$HOME/$default_folder"
      fi
    fi

    install_label=$(echo $1 | tr a-z A-Z)

    install_confirmed=false
    while [ $install_confirmed = "false" ]; do
      _log "Customize the destination folder or press [ENTER] to accept the default shown below." $YELLOW "\n" "\n"
      read -p "Default [$default_install_path]: " chosen_install_path
      chosen_install_path=${chosen_install_path:-$default_install_path}
      chosen_install_path=$(echo "${chosen_install_path%/*}")
      chosen_install_path=$(echo "${chosen_install_path%/$default_folder*}/$default_folder")
      _log "Are you sure these details look correct?" $YELLOW "\n" "\n"
      _log "Installation Path:       $chosen_install_path" "" "" "\n\n"

      case `init_select "Yes" "No"` in
        0) install_confirmed=true;;
        1) install_confirmed=false;;
      esac

      if [ $install_confirmed = "true" ]; then
        #TODO: Validate the path with a test -d constructor
        #TODO: If the folder exists and contains an existing GDK offer (a) delete and reinstall or (b) continue setup with that path
        #TODO: Expand the "~" if they used one for $HOME
        #TODO: Can we get some bash completion on the folder paths?
        #TODO: Check for 'gsk-conf.sh' instead of hard-coding the host and port values
        #TODO: Show all configuration settings here for review before executing
        #TODO: Figure out how to get '--job=4' bumped up without manually installing the gems here
        chosen_parent_path=$(echo "${chosen_install_path%/*}")
        if [ -z $chosen_parent_path ]; then
          chosen_parent_path="/"
        fi

        #TODO: Write the "chosen_parent_path" into the config file as USER_PROJECTS_PATH

        _log "Installing GitLab $install_label..." $PURPLE "\n" ""
        cd $chosen_parent_path
        gdk init $default_folder
        cd $chosen_install_path
        echo $1.gitlab > hostname
        echo 127.0.0.1 > host

        if [ $1 = "ee" ]; then
          echo 3001 > port
          echo 3809 > webpack_port
          echo 3011 > gitlab_pages_port

          #TODO: Research if we should have these on seperate ports vs an alternate database name
          echo 5432 > postgresql_port
          echo 5432 > postgresql_geo_port
          echo 5000 > registry_port

        elif [ $1 = "ce" ]; then
          echo 3000 > port
          echo 3808 > webpack_port
          echo 3010 > gitlab_pages_port

          #TODO: Research if we should have these on seperate ports vs an alternate database name
          echo 5432 > postgresql_port
          echo 5432 > postgresql_geo_port
          echo 5000 > registry_port
        fi
        gdk install gitlab_repo=https://gitlab.com/gitlab-org/gitlab-$1.git
      fi
    done

    if [ $1 = "ee" ]; then
      install_choice "ce"
    else
      exit_final_handler
    fi
  }

  exit_final_handler() {
    _log "Setup is complete!" $GREEN "\n" "\n"
    exit $?
  }

  exit_handler() {
    if [ $? -eq 100 ]; then
      _error "Installation has failed. Please review the output above for more information."
    else
      _log "Requirements post-install version logs" $CYAN "\n" ""
      _hr
      log_versions
      install_choice "ee"
    fi
  }

  trap exit_handler EXIT

  _log " MACOS" "$NORMAL" "" "\n" "" 1
  ENABLE_LIVERELOAD=1
  CI_BUILD_REF_NAME=master
  RUNLEVEL=1

  # Allow skipping all requirements checks for debugging purposes with "gsk macos debug"
  if ! [ -z $1 ] && [ $1 == "dev" ]; then
    _log "DEV MODE: Skipping all requirements checking..." $YELLOW " " " "
    exit_handler
  fi

  _log "Checking environment..." $CYAN "" ""
  _hr
  if [ $USER == "root" ]; then
    _error "You must use a non-root user to install the GDK."
    (exit 100)
    exit_handler
  fi

  if [ $USER_PROFILE == "${HOME}/.bash_profile"]; then
    reqfile1="${HOME}/.profile"
    reqfile2="${HOME}/.bash_profile"

    test -f $reqfile1
    reqval1=$?

    test -f $reqfile2
    reqval2=$?

    if [ $reqval1 -eq 1 ] || [ $reqval2 -eq 1 ]; then
      _log "How to create ~/.bash_profile and ~/.profile\n\n\thttps://apple.stackexchange.com/questions/99835/how-to-create-bash-profile-and-profile" $YELLOW "" ""
      _log "What's the difference between .bashrc, .bash_profile, and .environment?\n\n\thttps://apple.stackexchange.com/a/99900" $YELLOW "" ""
      _error "Your '$reqfile1' and/or '$reqfile2' files do not exist. On maOS, you will need these files. Please do some reading, set it up manually, and try again afterwards."
      (exit 100)
      exit_handler
    fi
  elif [ $USER_PROFILE == "${HOME}/.zshrc"]; then
      _error "Your '$USER_PROFILE' files does not exist. Please check your shell configuration and try again afterwards."
      (exit 100)
      exit_handler
  fi

  # declare -a arr=("/usr/local/sbin")
  # for i in "${arr[@]}"
  # do
  #   if [[ ":$PATH:" != *":$i:"* ]]; then
  #     _log "Adding PATH location: $i"
  #     PATH="${PATH:+${PATH}:}$i"
  #   fi
  # done
  echo 'export PATH="/usr/local/sbin:$PATH"' >> $reqfile2
  source $reqfile2

  _log "Environment is ready." $GREEN "" ""

  # TODO: Install homebrew with: ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
  _log "Requirements pre-install version logs" $CYAN "\n" ""
  _hr
  log_versions

  _log "Updating and cleaning brew..." $CYAN "\n" ""
  _hr
  brew pin node@12
  brew update
  brew cleanup --verbose

  # Uncomment to diagnose your own problems
  # brew doctor --verbose

  # Emergency fix procedure for brew
  # rm -rf /usr/local/Homebrew/.git
  # git init
  # git remote add origin https://github.com/Homebrew/brew
  # git fetch --all --verbose
  # git reset --hard origin/master
  # git checkout -B master origin/master
  # check the symlink for this shell script: cat /usr/local/bin/brew
  # if not intact do: ln -s /usr/local/Homebrew/bin/brew /usr/local/bin

  _log "Installing required packages..." $CYAN "\n" ""
  _hr
  pkg_list=("git"
            "nvm"
            "gpg"
            "yarn"
            "rbenv"
            "go"
            "postgresql@10"
            "redis"
            "libiconv"
            "cmake"
            "coreutils"
            "re2"
            "graphicsmagick"
            "runit"
            "exiftool"
            "gitlab-runner"
            )

  cask_list=("google-chrome"
             "chromedriver"
             )

  for pkg in "${pkg_list[@]}"; do
    eval "$( brew info "${pkg}" 2> >(setval errval) > >(setval stdval))"

    stdval=$(echo $stdval | tr -cd '[:alnum:] ')
    stderr=$(echo $stderr | tr -cd '[:alnum:] ')

    # Uncomment for debugging after potential changes in brew behavior or message syntax
    #echo -e "\nstdval:\n $stdval\n\n"
    #echo -e "\nerrval:\n $errval\n\n"

    if [ -z "$errval" ]; then
      #not_installed=$(echo $stdval | grep -m 1 -c "Not installed" || exit 2)
      if $(echo -e $stdval | grep "Not installed"); then
      #if [ $not_installed -eq 1 ]; then
        if [ $pkg == "go" ]; then
          _log "Preparing system to install '$pkg'..." $PURPLE "" ""
          echo 'export GOPATH="${HOME}/.go"' >> $reqfile2
          echo 'export GOROOT="$(brew --prefix golang)/libexec"' >> $reqfile2
          echo 'export PATH="${GOPATH}/bin:${GOROOT}/bin:$PATH"' >> $reqfile2
          source $reqfile2
          test -d "${GOPATH}" || mkdir "${GOPATH}"
          test -d "${GOPATH}/src/github.com" || mkdir -p "${GOPATH}/src/github.com"
        fi

        pkgargs=""

        if [ $pkg == "yarn" ]; then
          pkgargs=" --ignore-dependencies"
        fi

        _log "Installing '$pkg'..." $PURPLE "" ""
        brew install ${pkg}${pkgargs}

        if [ $pkg == "postgresql@10" ]; then
          echo 'export PATH="/usr/local/opt/postgresql@10/bin:$PATH"' >> $reqfile2
          source $reqfile2

          # @TODO: Remove these lines as soon as possible, homebrew is supposed to handle some of this for us
          # ln -sfv /usr/local/opt/postgresql/*.plist ~/Library/LaunchAgents
          # alias pg_start="launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist"
          # alias pg_stop="launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist"
        fi
      else
        _log "Not installing '$pkg' because brew has already installed it." "" ""
      fi

      if [ $pkg == "rbenv" ]; then
        cfg_line='eval "$(rbenv init -)"'
        cfg_count=$(grep -c "$cfg_line" $reqfile2)
        if [ $cfg_count -lt 1 ]; then
          _log "Configuring '$pkg'..." $PURPLE "" ""
          echo $cfg_line >> $reqfile2
          source $reqfile2

          # https://gitlab.com/gitlab-org/gitlab-ee/blob/master/.ruby-version
          # TODO: move this out a level and check against "rbenv version" > "system (set by /Users/dietrichstein/.rbenv/version)" first
          ruby_version="2.6.3"
          rbenv install $ruby_version

          _log "Installing 'Ruby $ruby_version'..." $PURPLE "" ""
          rbenv global $ruby_version

          # TODO: move this out of the loop and check the installed version
          bundler_version="1.17.3"
          _log "Installing 'Bundler $bundler_version'..." $PURPLE "" ""
          gem install bundler -v $bundler_version --force --no-document
          bundle config build.eventmachine --with-cppflags=-I/usr/local/opt/openssl/include

          # TODO: move this out of the loop and check the installed version
          gem_gdk="gitlab-development-kit"
          if [ $(gem list -i "^$gem_gdk$") = "false" ]; then
            _log "Installing '$gem_gdk' gem..." $PURPLE "" ""
            gem install gitlab-development-kit
          fi
        fi
      fi

      if [ $pkg == "nvm" ]; then
        if ! [ -d "$HOME/.nvm" ]; then
          _log "Configuring '$pkg'..." $PURPLE "" ""
          mkdir $HOME/.nvm
          echo 'export NVM_DIR="${HOME}/.nvm"' >> $reqfile2
          echo '[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"' >> $reqfile2
          echo '[ -s "/usr/local/opt/nvm/etc/bash_completion" ] && . "/usr/local/opt/nvm/etc/bash_completion"' >> $reqfile2
          source $reqfile2
          nvm install 12.4.0
        fi
      fi

      if [ $pkg == "gitlab-runner" ]; then
        _log "Configuring '$pkg'..." $PURPLE "\n" ""
        echo $(brew services start gitlab-runner)
      fi
    else
      _error "Invalid brew package: ${pkg}" && (exit 100)
    fi
  done

  for cask in "${cask_list[@]}"; do
    eval "$( brew cask info "${cask}" 2> >(setval errval) > >(setval stdval))"

    # Uncomment for debugging after potential changes in brew behavior or message syntax
    #echo -e "\nstdval:\n $stdval\n\n"
    #echo -e "\nerrval:\n $errval\n\n"

    if [ -z "$errval" ]; then
      not_installed=$(echo $stdval | grep -c "Not installed")
      if [ $not_installed -gt 0 ]; then
        _log "Installing '$cask'..." $PURPLE "" ""
        brew cask install "${cask}"
      fi
    fi
  done

  (exit 0)
}

"$@"
