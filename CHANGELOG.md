# CHANGELOG

This Changelog tracks major changes to the GitLab Sandbox Kit, such as dependency updates and new features.

## 2019-06-25

- Improved multi-stage builds by re-working the names, adding COPY commands, writing tagged images for each stage, and printing build times to the terminal

## 2019-06-24

- Renamed project to "GitLab Sandbox Kit"

## 2019-06-13

- Added support for semi-automated registration of GitLab Runner
- Converted the Dockerfile to multi-stage
