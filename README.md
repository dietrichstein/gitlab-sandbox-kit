# GitLab Sandbox Kit (GSK)

## Overview

Anyone can contribute to GitLab. Thanks to incredible efforts of the many who have, there are now several different paths available that make it easier for those who are just starting out. Each path brings both positive and negative aspects that are highly dependent upon the primary operating system being used.

This kit provides a path that aims to be highly automated for contributors using either **_macOS Mojave_** or **_Windows 10_** as their primary operating system. There are two features that make GSK the fastest and most automated setup process available for those environments.

### Docker

While the GSK is dependent upon GDK, it encapsulates the sandboxed enviroment within a docker container that runs from a set of multi-stage docker images. This features enables the setup process to be highly platform-agnostic and insulates developers from system-level changes that could otherwise affect their productivity.

### GSK Command-Line Utility

<table border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td>Description</td>
    <td>
      Provides a CLI with commands to build and run docker images running the <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/README.md">GitLab Development Kit (GDK)</a>
    </td>
  </tr>
    <td>Technology</td>
    <td>Primarily utilizes Bash and Docker and depends heaviliy upon the GDK.</td>
  </tr>
  <tr>
    <td valign="top">Environments</td>
    <td>
      <table align="left" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            macOS Mojave
          </td>
          <td>
            <ul>
              <li></li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            Windows 10
          </td>
          <td>
            <ul>
              <li>Requires Windows 10, version 1903 or higher</li>
              <li>Requires <a href="https://docs.microsoft.com/en-us/windows/wsl/install-win10">Windows Subsystem for Linux (WSL)</a> with an Ubuntu 18.04 distribution
              <li>Requires Docker for Windows</li>
              <li>Requires Hyper-V enabled, although containers still use process isolation</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>Recommendation</td>
    <td>The GSK is the easiest option available for anyone wishing to contribute with either a macOS or Windows 10 operating system.</td>
  </tr>
  <tr>
    <td>Caveats</td>
    <td>
      <ul>
        <li>Read and write performance is sacrificed (especially under WSL) due to the dependence upon Docker containers; but this is not intended for use in production enviroments anyway.</li>
      </ul>
    </td>
  </tr>
</table><br>

## Contributing to GitLab Sandbox Kit

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## Design Goals

- Enable users on supported environments to easily contribute using GDK
- Consolidate user-customizable configuration settings into single location
- Automate as much as possible; the struggle is real

## Installation

Please refer to the appropriate installation guide for your operating system.

[GSK Installation For macOS](doc/install_macos.md)

[GSK Installation for Windows 10](doc/install_windows.md)

# Getting Started

### Alias the GSK CLI (optional)

Creating an command-line alias for the GSL isn't required, but it will make it much easier for you to work with the GSK on regular basis. With an alias installed you won't have to type out `./gsk.sh <command>` each time you want to use the CLI. Instead, you can just type `gsk <command>` and get things done more quickly.

Feel skip this step entirely if you don't mind the extra typing or even just edit your shell profile manually if you know how. Before you can run the `alias` command you must first tell your operating system that the GSK script is executable using the following command:

```
sudo chmod a+x ./gsk.sh
```

Note: If this command fails for you then you should refer to the "Preparing Your Login Shell" section within the installation document for your operating system.

Run the `alias` command to have your shell profile updated with the alias:

```
./gsk.sh alias
```

> Note: that this currently lacks auto-detection to compensate for the many different shells available. That means this is only likely to work if you're using a bash shell. So, if you aren't using bash you should skips this step and go read up on how to set an alias in whatever shell you have installed.

Next, exit and reopen your terminal or simply open a new tab to work from. Finally, test that your alias is working using the following comand:

```
gsk help
```

### Building the docker images

Now that your have the CLI for GSK installed you can build your docker image. This step is going to take awhile so to be sure to read this section carefully so that you don't waste any time.

Before running the the `build` command you need to make sure that your user-configurable variables are all properly set at the top of the GSK script within the `gsk-conf.sh` file.

Also, take a look at the the file `gsk-packages-2-convenience.txt` and uncomment or add the package names for any software that you require if you don't want to have to reinstall those packages manually after every subsequent use of the `build` command. You can add or remove packages here later without causing the build process to start over and rebuild all of the image layers from scratch.

Once you have properly prepared, run the following command:

```
gsk build
```

### Use GDK normally once inside the container

Once you are logged in with no errors reported you can use GDX normally. For instance to start everything for CE you can run the following:

```
cd ~/gitlab-ce
gdk run
```

Similarly, for EE you can run

```
cd ~/gitlab-ee
gdk run
```

Once your CPU usage is settled down check the log output to confirm that there are some lines at the bottom that look something like this:

```
23:36:06 rails-web.1             | I, [...]  INFO -- : unlinking existing socket=/home/gdk/gitlab-ce/
23:36:06 rails-web.1             | I, [...]  INFO -- : listening on addr=/home/gdk/gitlab-ce/
23:36:06 rails-web.1             | I, [...]  INFO -- : master process ready
23:36:06 rails-web.1             | I, [...]  INFO -- : worker=0 ready
23:36:06 rails-web.1             | I, [...]  INFO -- : worker=1 ready
```

If you have not modified the ports within the `dockerfile` the following URLs will apply.

#### CE

http://localhost:3000

http://ce.gitlab.dev:3000

#### EE

http://localhost:3001

http://ce.gitlab.dev:3001

---

## Alternatives

### [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/README.md)

<table border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td>Description</td>
    <td>Provides a CLI with commands to install and configure projects and services.</td>
  </tr>
    <td>Technology</td>
    <td>Primarily utilizes Ruby, Bash, and Go to compile and orchestrate a wide variety of services.</td>
  </tr>
  <tr>
    <td valign="top">Environments</td>
    <td>
      <table align="left" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/vagrant.md">Vagrant</a>
          </td>
          <td>
            <ul>
              <li>Docker (Ubuntu) provider for use under Linux</li>
              <li>VirtualBox (Ubuntu) provider for use under Windows and OSX</li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/kubernetes.md">Minikube (Kubernetes)</a>
          </td>
          <td>
            <ul>
              <li>Expiremental support for deployment</li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md">macOS</a>
          </td>
          <td>
            <ul>
              <li>Native installation is supported for OS X 10.9 (Mavericks) and up</li>
              <li>Supports both MacPorts and Homebrew installation methods</li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md#ubuntu">Ubuntu</a>
          </td>
          <td><ul><li>Native installation is supported</li></ul></td>
        </tr>
        <tr>
          <td>
            <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md#arch-linux">Arch Linux</a>
          </td>
          <td><ul><li>Native installation is supported</li></ul></td>
        </tr>
        <tr>
          <td>
            <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md#debian">Debian</a>
          </td>
          <td><ul><li>Native installation is supported</li></ul></td>
        </tr>
        <tr>
          <td>
            <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md#fedora">Fedora</a>
          </td>
          <td><ul><li>Native installation is supported</li></ul></td>
        </tr>
        <tr>
          <td>
            <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md#centos">CentOS</a>
          </td>
          <td><ul><li>Native installation is supported</li></ul></td>
        </tr>
        <tr>
          <td>
            <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md#opensuse">OpenSUSE</a>
          </td>
          <td><ul><li>Native installation is supported</li></ul></td>
        </tr>
        <tr>
          <td>
            <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md#freebsd">FreeBSD</a>
          </td>
          <td><ul><li>Native installation is supported</li></ul></td>
        </tr>
        <tr>
          <td>
            <a href="">Windows 10</a>
          </td>
          <td>
            <ul>
              <li>Native installation is not possible and probably never will be</li>
              <li>Expiremental support for installation via the Windows Subsystem for Linux (WSL)</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>Recommendation</td>
    <td>With a native installation this is the best option for any contributor except for those with a  Windows 10 operating system. It is the fastest performing, most actively developed, and best supported solution available.</td>
  </tr>
  <tr>
    <td>Caveats</td>
    <td>
      <ul>
        <li>Requires <a href="https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md">manual installation</a> of extensive prerequisites</li>
        <li>Native installation on environments other than Ubuntu and macOS are not as well supported</li>
      </ul>
    </td>
  </tr>
</table>

### [GitLab Compose Kit](https://gitlab.com/gitlab-org/gitlab-compose-kit/tree/master)

<table border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td>Description</td>
    <td>Provides a CLI via Makefile commands to build and orchestrate containerized microservices providing all of GitLabs various dependencies.</td>
  </tr>
    <td>Technology</td>
    <td>
      <ul>
        <li>Primarily utilizes GNU Make and Bash in conjunction with Docker Compose and Docker Engine.</li>
        <li>Provides intra-container networking
        <li>Provides a workflow for switching between GitLab EE and GitLab CE development
        <li>Supports <code>lsyncd</code> for remote development (control and edit locally, run remotely)</li>
        <li>Performance is very good under native installation on Linux distributions</li>
        <li>Performance suffers under macOS due to virtualization of the file system</li>
        <li>Performance under Windows 10 with WSL is untested</li>
        <li>Does not include GDK as dependency or utilize GDK source code</li>
        <li>Geo is not yet supported</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td valign="top">Environments</td>
    <td>
      <table align="left" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <a href="">Linux</a>
          </td>
          <td>
            <ul>
              <li>Wide support for various distributions due to heavy dependence upon containers</li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            <a href="">macOS</a>
          </td>
          <td>
            <ul>
              <li>Write <a href="https://gitlab.com/gitlab-org/gitlab-compose-kit#performance">performance</a> is slow but but <a href="https://docs.docker.com/docker-for-mac/osxfs-caching/#performance-implications-of-host-container-file-system-consistency">optimization</a> is possible</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>Recommendation</td>
    <td>Due to the fast installation process and simple workflow for switching between EE and CE this is a very attactive option for anyone wishing to contribute with a Linux-like operating system.</td>
  </tr>
  <tr>
    <td>Caveats</td>
    <td>
      <ul>
        <li>Installation under Linux distributions other than Ubuntu may require undocumented problem-solving.</li>
      </ul>
    </td>
  </tr>
</table><br>

## Future Enhancements

### Argument-based branched docker image builds

Currently, the `Dockerfile` builds images with both EE and CE installed. As a result of this it isn't currently possible to choose to install just one or the other without modifying the `Dockerfile` or creating a separate one and configuring build to use that one instead. In theory, it should be possible to leverage build-time arguments in order to make this kind of choice possible. For instance, by passing `--build-arg INSTALL=ce`, `--build-arg INSTALL=ce`, or `--build-arg INSTALL=all` we should be able to speed up the build time significantly when only one distribution is desired. Consider this hypothetical example:

```
FROM gsk-runner AS install-ee
RUN gdk install gitlab_repo=https://gitlab.com/gitlab-org/gitlab-ee.git

FROM gsk-runner AS install-ce
RUN gdk install gitlab_repo=https://gitlab.com/gitlab-org/gitlab-ce.git

FROM gsk-runner AS install-all
RUN gdk install gitlab_repo=https://gitlab.com/gitlab-org/gitlab-ee.git
RUN gdk install gitlab_repo=https://gitlab.com/gitlab-org/gitlab-ce.git

FROM install-${INSTALL} AS install-done

FROM install-done AS gsk-convenience
RUN …
```

Docker should resolve the dependencies for this automatically due to the `FROM-${INSTALL}` directive.

### Shell auto-detection and other improvement for the `gsk alias` command

It would be a lot nicer if the `gsk alias` command would detect `zsh`, `ksh`, `fish` and possibly others so that it could execute successfully in those environments. Additionally it would be ideal if this command ran checks against the various profile files and did a few things such as:

- Only adding the alias if doesn't exist in the file or any other standard files
- Allowing the user to select an existing file in which to add the alias
- Prompting the user to create profile files if none already exist

### Automatic runner token extraction

It might be possible use existing code to extract the runner token without requiring developers to load the application in a browser in order to copy it out and past it into their `gsk-conf.sh` file.

### Automated testing

It would be very beneficial to have automated tests available so that changes to the scripts and the Dockerfile can be automatically checked under various host operating systems.

### Better `run_once` mechanisms

The `run_once` function is intended to only runs once while the user is still `root` in order to perform some system optimizations for the sandbox during run-time. It seems to be working, or it was working well at some point, but visibility on this is poor due to restrictions on the shell. Perhaps it would be better to remove this function and get it working by executing it from a separate script invoked from the `Dockerfile`? Some efforts were expended on this strategy initially using `ENTRYPOINT` and `CMD` patterns but that direction was not proving suitably flexible for our use-case as sandbox.

### Auto-detect running container and fall back to `shell()` function

It would be a lot nicer if the developer could always just run `gsk run` and not have to worry about this ki

### Run-time arguments to enable a user-customizable `tmux` layout

New seond-level command options like `gsk run tmux` and `gsk shell tmux` could bring up a shell reading tmux configuration files in order to drop the user into an immediately productive state with at least the following panes:

- Runner output
- Real-time system utilization and services status
- A shell prompt ready for working within `~/gitlab-ee`
- A shell prompt ready for working within `~/gitlab-ce`

### Optimize disk usage

The current `COPY / /` directives are brining over way too much from one image to the next. Probably, all that is needed to optimise this is to use `/home/gdk` as the sole copy location. If that doesn't drastically reduce the image sizes then it may be necessary to inspect folder sizes at each stage and evaluate cache clearing commands for the heaviest locations.

## License

The GitLab Sandbox Kit is distributed under the MIT license, see the [`LICENSE`](LICENSE) file.
