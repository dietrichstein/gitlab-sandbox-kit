[
  -t 'EE' 'zsh --login -i -c "cd ${USER_PROJECTS_PATH}/gdk-ee; zsh --login"' :
  -t 'GDK' 'zsh --login -i -c "cd ${USER_PROJECTS_PATH}/gdk-ee; DEV_SERVER_LIVERELOAD=true gdk run; zsh --login"' :
  -t 'RUNNER' 'zsh --login -i -c "cd ${USER_PROJECTS_PATH}/gdk-ee; gsk macos ee-runner; zsh --login"' :
  -t 'HTOP' 'htop'
]
