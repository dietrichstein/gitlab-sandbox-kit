[
  -t 'CE' 'zsh --login -i -c "cd ${USER_PROJECTS_PATH}/gdk-ce; zsh --login"' :
  -t 'GDK' 'zsh --login -i -c "cd ${USER_PROJECTS_PATH}/gdk-ce; DEV_SERVER_LIVERELOAD=true gdk run; zsh --login"' :
  -t 'RUNNER' 'zsh --login -i -c "cd ${USER_PROJECTS_PATH}/gdk-ce; gsk macos ce-runner; zsh --login"' :
  -t 'HTOP' 'htop'
]
